package org.example.rw.wu;

import java.util.Objects;

public class Human {
    private String name;
    private String secondName;
    private String surname;

    public Human(String name, String secondName, String surname) {
        this.name = name;
        this.secondName = secondName;
        this.surname = surname;

    }

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFullName() {
        return secondName == null
                ? String.format("%s %s", name, surname)
                : String.format("%s %s %s", name, secondName, surname);
    }

    public String getShortName() {
        return surname + " " + name.charAt(0) + "." + (secondName == null ? "" : secondName.charAt(0) + ".");

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return Objects.equals(name, human.name) && Objects.equals(secondName, human.secondName) && Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, secondName, surname);
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", secondName='" + secondName + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
