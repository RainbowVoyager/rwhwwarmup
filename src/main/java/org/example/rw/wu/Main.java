package org.example.rw.wu;

public class Main {

    public static void main(String[] args) {
        Human human = new Human("Vasiliy", "Ivanovich", "Pupkin");
        String fullName = human.getFullName();
        String shortName = human.getShortName();
        System.out.println(fullName);
        System.out.println(shortName);
    }
}

